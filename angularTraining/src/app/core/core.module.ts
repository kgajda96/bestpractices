import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { HttpCoreService } from './services/http-core.service';

@NgModule({
  declarations: [
  ],
  imports: [
      HttpClientModule
  ],
  providers: [HttpCoreService],
})
export class CoreModule { }
