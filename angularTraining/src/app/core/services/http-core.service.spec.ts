import { TestBed } from '@angular/core/testing';

import { HttpCoreService } from './http-core.service';

describe('HttpCoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttpCoreService = TestBed.get(HttpCoreService);
    expect(service).toBeTruthy();
  });
});
