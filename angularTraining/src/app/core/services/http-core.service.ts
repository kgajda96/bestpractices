import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_PATH_CONFIG } from 'src/app/utils/const/path-api.const';

@Injectable()
export class HttpCoreService {
  apiUrl = API_PATH_CONFIG.BASE_PATH;

  constructor(private httpClient: HttpClient) {}

  get<T>(path: string, params?: Map<string, string[] | string>): Observable<T> {
    const httpParams = this.resolveParams(params);
    return this.httpClient.get<T>(`${this.apiUrl}${path}`, httpParams);
  }

  post<T>(path: string, body: any): Observable<T> {
    return this.httpClient.post<T>(`${this.apiUrl}${path}`, body);
  }

  private resolveParams(params?: Map<string, string[] | string>): {} {
    let httpParams = new HttpParams();
    if (params) {
        params.forEach((value, key) => {
            if (Array.isArray(value)) {
                value.forEach(el => {
                    httpParams = httpParams.append(key, el);
                });
            } else {
                httpParams = httpParams.set(key, value);
            }
        });
    }
    return { params: httpParams };
}
}
