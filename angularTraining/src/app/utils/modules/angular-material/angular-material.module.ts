import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  imports: [MatTableModule, MatCardModule],
  exports: [MatTableModule, MatCardModule],
})
export class AngularMaterialModule {}
