import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BeerContainerComponent } from './components/beer-container/beer-container.component';


const routes: Routes = [
    {
        path: '',
        component: BeerContainerComponent,
    },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BeerRoutingModule { }
