import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpCoreService } from 'src/app/core/services/http-core.service';
import { API_PATH_CONFIG } from 'src/app/utils/const/path-api.const';
import { BeerList } from '../models/beer.model';

@Injectable()
export class BeerHttpService {
    beerUrl = API_PATH_CONFIG.BEER_PATH;

  constructor(private httpCoreService: HttpCoreService) { }

  getListOfBeers(params: Map<string, string[] | string>): Observable<BeerList[]> {
    return this.httpCoreService.get<BeerList[]>(this.beerUrl, params);
  }
}
