import { TestBed } from '@angular/core/testing';

import { BeerHttpService } from './beer-http.service';

describe('BeerHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BeerHttpService = TestBed.get(BeerHttpService);
    expect(service).toBeTruthy();
  });
});
