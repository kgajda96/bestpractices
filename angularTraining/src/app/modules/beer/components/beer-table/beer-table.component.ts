import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BeerList } from '../../models/beer.model';
import { BeerHttpService } from '../../services/beer-http.service';

@Component({
  selector: 'app-beer-table',
  templateUrl: './beer-table.component.html',
  styleUrls: ['./beer-table.component.scss'],
})
export class BeerTableComponent implements OnInit {
  listOfBeer$: Observable<BeerList[]>;
  displayedColumns = ['id', 'name', 'description'];

  constructor(private beerHttpService: BeerHttpService) {}

  ngOnInit(): void {
    this.getBeerList();
  }

  // ES6
  getBeerList = () =>
    (this.listOfBeer$ = this.beerHttpService.getListOfBeers(
      this.createPageParams('1', '20')
    ))

  createPageParams(page: string, size: string): Map<string, string[] | string> {
        const params = new Map<string, string>();
        params.set('page', page);
        params.set('per_page', size);
        return params;
  }
}

