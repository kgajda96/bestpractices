import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BeerContainerComponent } from './components/beer-container/beer-container.component';
import { BeerHttpService } from './services/beer-http.service';
import { BeerRoutingModule } from './beer-routing.module';
import { BeerTableComponent } from './components/beer-table/beer-table.component';
import { AngularMaterialModule } from 'src/app/utils/modules/angular-material/angular-material.module';



@NgModule({
  declarations: [BeerContainerComponent, BeerTableComponent],
  imports: [
    CommonModule,
    BeerRoutingModule,
    AngularMaterialModule
  ],
  providers: [BeerHttpService]
})
export class BeerModule { }
