import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

const routes: Routes = [
    {
      path: '',
      redirectTo: 'beer',
      pathMatch: 'full'
    },
    {
        path: 'beer',
        loadChildren: () => import('./modules/beer/beer.module').then(m => m.BeerModule)
    }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
